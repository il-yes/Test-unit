<?php

namespace UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use UserBundle\Entity\User;

class PersonneTest extends WebTestCase
{
    private $_personne;

    protected function setUp()
    {
        $this->_personne = new User('Florentina');
    }

    public function assertPreConditions()
    {
        $this->assertEquals('Florentina', $this->_personne->getUsername());
    }

    /**
     * @test
     */
    public function personneChangeEtage()
    {
        $client = static::createClient();

        $this->_personne->setEtage(7);
        $this->assertEquals(6, $this->_personne->getEtage());

        $crawler = $client->request('GET', '/personne');

        $this->assertContains('Hello World !!!', $client->getResponse()->getContent());
    }
}
