<?php
/**
 * Created by PhpStorm.
 * User: versus
 * Date: 13/08/17
 * Time: 14:08
 */

namespace UserBundle\Listener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent as BaseFormEvent;


class RegistrationListener implements EventSubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return array(
          FOSUserEvents::REGISTRATION_SUCCESS => 'on_registration_success',
          //FormEvents::PRE_SUBMIT => 'on_pre_submit'
        );
    }

    public function on_pre_submit(BaseFormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();
        die(dump($form));
    }

    public function on_registration_success(FormEvent $event)
    {
        //die(dump($event));
        $role_corporate = array("ROLE_CORPORATE");
        $role_civil= ["ROLE_CIVIL"];

        $user = $event->getForm()->getData();

        $user->setRoles($role_corporate);
    }
}