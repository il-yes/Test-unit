<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }


    public function testDrivenAction()
    {
        return $this->render('UserBundle:Personne:index.html.twig');
    }
}
