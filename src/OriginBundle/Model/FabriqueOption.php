<?php
/**
 * Created by PhpStorm.
 * User: versus
 * Date: 20/08/17
 * Time: 14:07
 */

namespace OriginBundle\Model;


use OriginBundle\Entity\OptionVehicule;

class FabriqueOption
{
    protected $options = [];

    public function getOption($nom)
    {
        if(array_key_exists($nom, $this->options)){
            $resultat = $this->options[$nom] = $nom;
        }else{
            $resultat = new OptionVehicule($nom);
            $this->options[$nom] = $resultat;
        }
        return $resultat;
    }
}