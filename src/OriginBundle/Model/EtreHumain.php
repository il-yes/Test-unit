<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\EtreCoureur;
use OriginBundle\Model\EtreNageur;

/**
 * EtreHumain
 *
 */
class EtreHumain extends EtreVivant implements EtreNageur, EtreCoureur, Bouger
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    protected $_nationalite;

    public function respirer() {
        echo 'Par le nez et/ou la bouche';
    }

    protected function _cesserFonctionsVitales() {
        echo 'le coeur s\'arrête de battre...AAAARGH';
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function nager()
    {
        // TODO: Implement nager() method.
    }

    public function courir()
    {
        // TODO: Implement courir() method.
    }

    public function se_deplacer()
    {
        // TODO: Implement se_deplacer() method.
    }
}

