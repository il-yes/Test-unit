<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;


interface NotificationInterface
{
    /**
     * Returns the user unique id.
     *
     * @return mixed
     */
    public function getId();

    public function getName();

    public function getMessage();

}

