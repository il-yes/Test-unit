<?php
/**
 * Created by PhpStorm.
 * User: versus
 * Date: 06/08/17
 * Time: 16:51
 */

namespace OriginBundle\Model;


Interface StrategyInterface
{
    public function talk($data);
}