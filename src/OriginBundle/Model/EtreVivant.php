<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 04/08/17
 * Time: 10:53
 */

namespace OriginBundle\Model;


abstract class EtreVivant
{
    protected $_age = 0;
    protected $_poids = 0;

    public function mourir() {
        $this->_cesserFonctionsVitales();
    }

    abstract public function respirer();
    abstract protected function _cesserFonctionsVitales();
}