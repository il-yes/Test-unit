<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 11:21
 */

namespace OriginBundle\Model;


interface PersonneInterface
{
    public function parlerDuTemps();
}