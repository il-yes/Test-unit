<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;



abstract class NotificationShop extends Container
                                implements NotificationInterface, ObservateurInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    private $sujet;

    private $_stockage;

    protected $container;

    /**
     * NotificationShop constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct();
        $this->container = $container;
    }


    /**
     * @return mixed
     */
    public function getStockage()
    {
        return $this->_stockage;
    }

    /**
     * @param mixed $stockage
     */
    public function setStockage($stockage)
    {
        $this->_stockage = $stockage;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return NotificationShop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return NotificationShop
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getCodeName()
    {
        return $this->codeName;
    }

    /**
     * Set codeName
     *
     * @param string $codeName
     *
     * @return NotificationShop
     */
    public function setCodeName($codeName)
    {
        $this->codeName = $codeName;

        return $this;
    }

    abstract protected function _displayMessage($custom);

    public function mettreAJour(SujetInterface $sujet, \stdClass $objet, $options)
    {
        $this->setStockage($options);
        $message = $this->_displayMessage($this->getStockage());
        //die(dump($this->add('info', $message)));

        echo  "<br /><div class='container notif-container'><h5>" . $sujet->getNom() ." : </h5>".  "<p class='notif-content'>" . $message.  "</p></div><br />";
        //return $this->container->get('twig')->render('OriginBundle:Notification:add.html.twig', compact('message'));

        /*
        foreach($objet as $attr => $valeur) {
            if (is_array($valeur)) {
                $valeur = implode(',', $valeur);
            }
            echo "$attr vaut $valeur".  "<br />", PHP_EOL;
        }
        */

    }


    public function add($type, $message)
    {
        // TODO: Implement add() method.
    }

    public function peek($type, array $default = array())
    {
        // TODO: Implement peek() method.
    }

    public function peekAll()
    {
        // TODO: Implement peekAll() method.
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function setAll(array $messages)
    {
        // TODO: Implement setAll() method.
    }

    public function keys()
    {
        // TODO: Implement keys() method.
    }
}