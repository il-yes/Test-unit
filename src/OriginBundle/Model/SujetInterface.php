<?php
/**
 * Created by PhpStorm.
 * User: versus
 * Date: 05/08/17
 * Time: 14:37
 */

namespace OriginBundle\Model;


interface SujetInterface {
    public function attacher(ObservateurInterface $observateur);
    public function detacher(ObservateurInterface $observateur);
    public function notifier();
}