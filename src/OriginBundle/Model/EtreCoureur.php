<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 04/08/17
 * Time: 11:24
 */

namespace OriginBundle\Model;


interface EtreCoureur
{
    public function courir();
}