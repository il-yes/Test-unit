<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flore
 *
 * @ORM\Table(name="flore")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\FloreRepository")
 */
class Flore extends EtreVivant implements Bouger
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    protected $_environnement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function se_deplacer()
    {
        // TODO: Implement se_deplacer() method.
    }

    public function respirer()
    {
        echo 'Par la photo-synthèse';
    }

    protected function _cesserFonctionsVitales()
    {
        echo "Plus d'amour, alors celle-ci se fane...";
    }
}

