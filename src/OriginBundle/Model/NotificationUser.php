<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;


abstract class NotificationUser extends Container implements NotificationInterface, ObservateurInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


}