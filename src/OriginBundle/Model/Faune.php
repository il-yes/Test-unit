<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Faune
 *
 * @ORM\Table(name="faune")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\FauneRepository")
 */
class Faune
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

