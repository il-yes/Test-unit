<?php

namespace OriginBundle\Model;

use Doctrine\ORM\Mapping as ORM;


abstract class Config
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var float
     *
     * @ORM\Column(name="percentage", type="float")
     */
    public $percentage;

    /**
     * @var int
     *
     * @ORM\Column(name="flat", type="integer")
     */
    public $flat;

    /**
     * @var array
     *
     * @ORM\Column(name="tranche", type="array")
     */
    public $tranche;

    /**
     * @var int
     *
     * @ORM\Column(name="minimum_fee", type="integer")
     */
    public $minimumFee;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param float $percentage
     *
     * @return Config
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set flat
     *
     * @param integer $flat
     *
     * @return Config
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;

        return $this;
    }

    /**
     * Get flat
     *
     * @return int
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * Set tranche
     *
     * @param array $tranche
     *
     * @return Config
     */
    public function setTranche($tranche)
    {
        $this->tranche = $tranche;

        return $this;
    }

    /**
     * Get tranche
     *
     * @return array
     */
    public function getTranche()
    {
        return $this->tranche;
    }

    /**
     * Set minimumFee
     *
     * @param integer $minimumFee
     *
     * @return Config
     */
    public function setMinimumFee($minimumFee)
    {
        $this->minimumFee = $minimumFee;

        return $this;
    }

    /**
     * Get minimumFee
     *
     * @return int
     */
    public function getMinimumFee()
    {
        return $this->minimumFee;
    }
}

