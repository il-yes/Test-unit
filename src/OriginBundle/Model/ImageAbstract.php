<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 12:41
 */

namespace OriginBundle\Model;


abstract class ImageAbstract
{
    protected $_cheminFichier;

    protected $_contenuFichier = '';

    protected $_tailleFichier;

    public function __construct($cheminFichier) {
        $this->_cheminFichier = $cheminFichier;
    }

    public function donnerTaille() {
        return $this->_tailleFichier;
    }

    public function aContenu() {
        return null != $this->_contenuFichier;
    }

    abstract public function afficherContenu();
}