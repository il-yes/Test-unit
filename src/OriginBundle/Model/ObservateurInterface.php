<?php
/**
 * Created by PhpStorm.
 * User: versus
 * Date: 05/08/17
 * Time: 14:37
 */

namespace OriginBundle\Model;


interface ObservateurInterface
{
    public function mettreAJour(SujetInterface $sujet, \stdClass $objet, $options);

}