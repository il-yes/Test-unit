<?php

namespace OriginBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetmessagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('getmessages')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $em = $this->getContainer()->get('doctrine')->getManager();
        $messages = $em->getRepository('OriginBundle:Messages')->findAll();
        
        $result = "";
        foreach($messages as $message){
         $result .= $message->getSubject().";".$message->getMessage()."\n";
        }
       
            
        $output->writeln($result);
    }

    // Pour afficher dans un fichier csv
    //php app/console getmessages > messages.csv
}

