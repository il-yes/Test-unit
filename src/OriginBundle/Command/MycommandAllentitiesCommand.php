<?php

namespace OriginBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

class MycommandAllentitiesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mycommand:allentities')
            ->setDescription(' Create a command that do entities and a schema update adter')
            ->addArgument('entity',  InputArgument::REQUIRED, 'bundle:entity or Bundle name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('entity');
        
        //execute entities
        $command = $this->getApplication()->find('doctrine:generate:entities');
        
        $arguments = array(
          'name' =>  $argument
            );
        
        $commandInput = new ArrayInput($arguments);
        
        $returnCode = $command->run($commandInput, $output);
        
        //do schema update 
        $command = $this->getApplication()->find('doctrine:schema:update');
        
        $argumentUpdate = array(
          '--force' => true
            );
        
       $commandInput = new ArrayInput($argumentUpdate);
       
       $returnCode = $command->run($commandInput, $output);
          
        $output->writeln('Command done.');
    }

}

