<?php

namespace OriginBundle\Controller;


use Gedmo\Exception\RuntimeException;
use OriginBundle\Entity\Clt;
use OriginBundle\Entity\Fan;
use OriginBundle\Entity\GestionnaireImage;
use OriginBundle\Entity\Humain;
use OriginBundle\Entity\InterpreteRusse;
use OriginBundle\Entity\NotifItem;
use OriginBundle\Entity\PersonnageInfluent;
use OriginBundle\Entity\Plante;
use OriginBundle\Entity\PresidentFrancais;
use OriginBundle\Entity\ProxyImage;
use OriginBundle\Entity\StandardImage;

use OriginBundle\Entity\Tarif;
use OriginBundle\Entity\TarifAtm;
use OriginBundle\Entity\TarifKyc;
use OriginBundle\Entity\TarifProduit;
use OriginBundle\Entity\VehiculeCommande;
use OriginBundle\Model\FabriqueOption;

use OriginBundle\Entity\Strategy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}",defaults={"_locale" = "fr"}, requirements={"_locale" = "fr|en"}, name="homepage")
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        //$session->getFlashBag()->add("info", "Opération validée : mise à jour effectuée !");
        //die(dump($session->getFlashBag()));
        $add_item = $this->container->getParameter('app.shop_item_add');
        $data = 'Salut';
        $strategy = new Strategy(new Plante($data));
        $pi = new PersonnageInfluent('lerdorf');

        $fan1 = new Fan();
        $fan2 = new NotifItem($this->container);
        $pi->attacher($fan1);
        $pi->attacher($fan2);
        $pi->setOptions($add_item);
        $pi->changerAge(36);
        $pi->changerHobbies(array("linux", "c++", "c"));
        $pi->notifier(); // Ne pas oublier !


        return $this->render('OriginBundle:Default:index.html.twig');
    }

    /**
     *
     * @Route("/{_locale}/eco-system",defaults={"_locale" = "fr"}, requirements={"_locale" = "fr|en"}, name="origin_eco")
     */
    public function eco_systemeAction()
    {
        /* ------------------------------- Proxy ------------------------------- */
        // 1er cas : L’interprète, un mandataire idéal !
        $presidentHote = new PresidentFrancais();
        $presidentHote->attacherInterprete(new InterpreteRusse());
        try {
            $presidentHote->discuterSurPerron();
        } catch (RuntimeException $exception)
        {
            echo "Allô, ici le chef du protocole !", PHP_EOL;
            echo "Le président vient de me dire '" .
                $exception->getMessage(), "'", PHP_EOL;
            echo "Vite, allez chercher un interprète !", PHP_EOL;
        }

        // 2eme cas : Le chargement fainéant
        $gestionnaireImage = new GestionnaireImage();
        $img = __DIR__.'/img/3.jpg';

        $image = new StandardImage($img);
        //dump($image, $img);
        $photoStd = $gestionnaireImage->traiterImage($image);

        $image = new ProxyImage($img);
        $photoPxy = $gestionnaireImage->traiterImage($image);
        //die(dump($image));
        //echo "<img  src=' ". $gestionnaireImage->traiterImage() ." ' >";


        /* ------------------------------- Observer ------------------------------- */
        $francais = new Humain('francais');
        $anglais= new Humain('anglais');
        dump($anglais->mourir());
        $planteTropicale = new Plante('plante verte tropicale');

        $objet = new \stdClass();
        $objet->test = 'ok mec !';

        $europeens = [$francais, $anglais];

/* ------------------------------- Flyweight ------------------------------- */
        // PORSCHE carrera
        /*
        $fabrique = new FabriqueOption();
        $vehicule1 = new VehiculeCommande('Porshe carrera');
        $vehicule1->ajouterOptions('air bag', 80, $fabrique);
        $vehicule1->ajouterOptions('direction assistée', 90, $fabrique);
        $vehicule1->ajouterOptions('vitres electriques', 85, $fabrique);
        dump($vehicule1);
        $vehicule1->afficheOptions();*/
        // Audi Rs8
        /*$fabrique = new FabriqueOption();
        $vehicule2 = new VehiculeCommande('Audi Rs8');
        $vehicule2->ajouterOptions('air bag', 150, $fabrique);
        $vehicule2->ajouterOptions('direction assistée', 90, $fabrique);
        $vehicule2->ajouterOptions('vitres electriques', 85, $fabrique);
        dump($vehicule2);
        $vehicule2->afficheOptions();*/

        $martin= new Clt('Martin');
        $tarif_kyc = new TarifKyc('Kyc Martin', 130);
        $tarif_Atm = new TarifAtm('Atm Martin', 100);
        $martin->attacher($tarif_kyc);
        $martin->attacher($tarif_Atm);

        $pierre= new Clt('Pierre');
        $tarif_Atm = new TarifAtm('Atm Pierre', 90);
        $pierre->attacher($tarif_Atm);

        foreach ($martin->configs as $el){
            if($el->nom == 'Cb'){
                dump($el);
            }
        }

        die(dump($martin, $pierre));

        $em = $this->getDoctrine()->getManager();
        $porsche = $em->getRepository('OriginBundle:VehiculeCommande')->find(2);
        dump($porsche);
        //$em->persist($vehicule1);
        //$em->persist($vehicule2);
        $em->flush();


        return $this->render('OriginBundle:Eco:index.html.twig', [
            'francais' => $francais,
            'anglais' => $anglais,
            'europeens' => $europeens,
            'objet' => $objet,
            'planteTropicale' => $planteTropicale,
            'photoStd' => $photoStd,
            'photoPxy' => $photoPxy,
        ]);
    }


    /**
     *
     * @Route("/{_locale}/network-followers",defaults={"_locale" = "fr"}, requirements={"_locale" = "fr|en"}, name="network")
     */
    public function networkObservateur()
    {
        $lerdorf = new PersonnageInfluent('lerdorf');
        $ferrandez = new Fan();
        $jean = new Fan();
        $notif = new NotifItem($this->container);
        $lerdorf->attacher($ferrandez);
        $lerdorf->attacher($jean);
        $lerdorf->changerAge(36);
        $lerdorf->changerHobbies(array("linux", "c++", "c"));
        $lerdorf->changerAge(37);
        $lerdorf->attacher($notif);
        $lerdorf->changerAge(40);
        //die(dump($lerdorf));
        $lerdorf->setOptions('add');
        $lerdorf->notifier();
        //die();

        return $this->render('OriginBundle:Observateur:network.html.twig');
    }





    /* ---------------------------------------------------------------------------------- */

    /* ------------------------------- Form errors handler ------------------------------- */

    public function getErrorMessages(FormInterface $form)
    {
        $errors = array();

        //this part get global form errors (like csrf token error)
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        //this part get errors for form fields
        /** @var Form $child */
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $options = $child->getConfig()->getOptions();
                //there can be more than one field error, that's why implode is here
                $errors[$options['label'] ? $options['label'] : ucwords($child->getName())] = implode('; ', $this->getErrorMessages($child));
            }
        }

        return $errors;
    }


    class CronController
    {
        /**
         * - Facturation : 
         *  initialisation de l'action par un pré traitement des infos sousmises
         *  exection de la facturation
         */
        public function billingAction(Request $request)
        {
            # Pré traitement facturation
            $processor = $this->preprocessing($request);
            $attachmentsInvoicing = $processor->attachmentsInvoicing;
            $invoicingPeriod = $processor->invoicingPeriod;


            $clientFactureItemBillingsByClients = $em
            ->getRepository('TechCorpBundle:clientFactureItemBillings')->getClientFactureItemBillingsByClients();


            # Execution facturation
            $invoiceBuilder = $this->billingExecution(
                $invoicingPeriodValidator, 
                $clientFactureItemBillingsByClients, 
                $attachmentsInvoicing['targetedClients']
            );
            

            $session->getFlashBag('info', $invoiceBuilder->getConclusions())
            return $this->redirectToRoute('billing_invoice_index');
        }

        /* - Retourne un objet StdClass : 
          StdClass Object<{init: void,isValid: boolean, _object: string}>
        */
        private function preprocessing($request)
        {
            $attachmentsInvoicing = $this->getAttachmentsInvoicing($request);
            
            $invoicingPeriod = $this->get->('billing.invoicing_period');
            $invoicingPeriodValidator = $this->getInvoicingValidatorPeriod($invoicingPeriod, $attachmentsInvoicing);

            if(!$invoicingPeriodValidator->_result){
                $request->get('session')->getFlashBag('warning', $invoicingPeriodValidator->_errorsMsg)
                return $this->redirectToRoute('billing_invoice_index');
            }

            return new \stdClass()[
                'attachmentsInvoicing' => $attachmentsInvoicing,
                'invoicingPeriod' => $invoicingPeriodValidator,
            ];
        }


        /**
         * - Retourne l'objet InvoicePeriod après contrôle de la période de facturation :
         *      
         */
        private function getInvoicingValidatorPeriod(
            InvoicingPeriod $invoicingPeriod, 
            $attachmentsInvoicing)
        {
            $invoicingPeriod->init($attachmentsInvoicing['startsAt'], $attachmentsInvoicing['endsAt']);
            return $invoicingPeriod->isvalid();
        }

        /*
        * - Retourne un array<{startsAt: datetime,endsAt: datetime, targetedClients: array}>
        *
        */
        private function getAttachmentsInvoicing($request)
        {
            $startsAtMonth = $request->request->get('startsAtMonth');
            $startsAtYear = $request->request->get('startsAtYear');
            $endsAtMonth = $request->request->get('endsAtMonth') ? $request->request->get('endsAtMonth') : nul;
            $endsAtYear = $request->request->get('endsAtYear') ? $request->request->get('endsAtYear') : nul;
            
            $dateHandler = $this->get('billing.date_handler');

            return [
                'startsAt' => $dateHandler->datetimeObjectConverter($startsAtMonth, $startsAtYear),
                'endsAt' => $dateHandler->datetimeObjectConverter($endsAtMonth, $endsAtYear),
                'targetedClients' => $targetedClients = $request->request->get('targetedClients');,
            ];
        }

        /**
         * - Execution de la facturation  
         *  Retourne l'objet InvoiceBuilder après execution.
         */
        private function billingExecution($period, $consumptions, $clients)
        {
            $invoiceBuilder = $this->get('billing.invoiceBuilder');
            $invoiceBuilder->init($period, $consumptions, $clients);
            $invoiceBuilder->invoicingConsumptions();

            return $invoiceBuilder;
        }
    }




    /**
    * 
    */
    class InvoiceBuilder
    {
        
        private $_container;

        private $_billingPeriod;

        private $_consumptions;

        private $_targetedClientsIdsToBill;

        private $_calculator;




        public function __init(InvoicingPeriod $period, $clientFactureItemBillings = null, $clientsIdsToBill = null)
        {
            $this->_billingPeriod = $period;
            $this->_consumptions = $clientFactureItemBillings;
            $this->_targetedClientsIdsToBill = $clientsIdsToBill;
            $this->_starterEntries = $this->getCountClientFactureItemBillings();
        }


        /**
         * I - Aiguillonne le moteur de facturation vers la bonne opération :
         *   facturation pour pour tous ou uniquement pour clients ciblés. 
         */
        public function invoicingConsumptions()
        {
            if($this->_targetedClientsIdsToBill != null){
                return $this->targetedOperation($this->_targetedClientsIdsToBill);
            }

            return $this->classicOperation($this->_consumptions);
        }


        /*
         * II.a - Opération ciblée
         *
         */
        private function targetedOperation($clientsIds)
        {
            // Clients
            $targetedClients = $this->getTargetedClients($clientsIds);

            return $this->targetedConsumptionsHandler($targetClients);
        }

        /*
         * II.b - Opération classique mensuelle
         *
         */
        private function classicOperation($consumptions)
        {    
            // ClientFacturesItemBillings
            $consumptionsByClients = $this->getConsumptionsByClients($consumptions); 

            return $this->consumptionsHandler($consumptionsByClients);
        }


        /**
         * II.a.1
         * - Gestionnaire des consommations ciblées
         */
        private function targetedConsumptionsHandler($clients)
        {
            foreach ($clients as $client) 
            {
                $consumptions = $client->getClientFactureItemBillings();
                $this->consumptionsHandler($consumptions);
            }
        }

        /**
         * III - 
         * - Gestionnaire des consommations classiques
         */

        public function consumptionsHandler($consumptions)
        {
            $periodConsumptions = $this->getPeriodConsumptions($consumptions);

            $this->billingConsumptions($periodConsumption);            
        }

        

        /**
         * IV - 
         * - Calcul des prix des consommations et de la facture et sauvegarde de celle-ci en base
         */

        private function billingConsumptions($consumptionsWithoutSellingPrices)
        {
            $consumptionsWithSellingPrices = $this->getConsumptionsWithSellingPrices($consumptionsWithoutSellingPrices);

            $clientBilling = $this->createClientBilling($consumptionsWithSellingPrices);

            $this->updateBillingElements($clientBilling, $consumptionsWithSellingPrices)

            $this->saveInvoice($clientBilling);
        }


        /*
         * III.a - Retourne les consommations de la periode de facturation
         *
         */
        private function getPeriodConsumptions($consumptions)
        {
            $tab = [];
            foreach ($consumptions as $consumption) 
            {
                $dateActivity = $this->getDate($consumption);

                if($this->_billingPeriod->endsAt != null){
                    $tab = $this->addBoundedConsumption($dateActivity, $tab, $consumption);
                }else{
                    $tab = $this->addMonthlyConsumption($dateActivity, $tab, $consumption);    
                }  
            }
            return $tab;
        }


        /**
         * III.a
         * - Retourne un tableau contenant les consommations de la période de facturation étalée sur 
         * plusieurs mois
         */
        private function addBoundedConsumption($dateActivity, $tab, $consumption)
        {   
            if($dateActivity >= $this->_billingPeriod->startsAt && 
               $dateActivity <= $this->_billingPeriod->endsAt ){
                array_push($tab, $consumption);
            }
            return $tab;
        }

        /**
         * III.a
         * - Retourne un tableau contenant les consommations de la période de facturation étalée sur 
         * un mois
         */
        private function addMonthlyConsumption($dateActivity, $tab, $consumption)
        {
            if($dateActivity == $this->_billingPeriod->startsAt ){
                array_push($tab, $consumption);
            }
            return $tab;
        }
    }



    /**
    * - Contrôle la validité de la période de facturation
    */
    class InvoicingPeriod
    {
        private $_startsAt;

        private $_endsAt;

        private $_errorsMsg;

        public $_result;

        function __construct(argument)
        {
            # code...
        }

        public function init(\Datetime $startAt, \Datetime $endsAt)
        {
            $this->_startsAt = $startAt;
            $this->_endsAt = $endsAt;
        }

        /*
         * - Contrôle la validité de la période de facturation
         *
         */
        private function isvalid()
        {
            // Période de plusieurs mois
            if($_endsAt != null)
            {
                if($this->_endsAt < $this->_startsAt)
                {
                    $this->_errorsMsg = $this->IncoherenceErrorMessage();
                    $this->_result = false;

                    return $this;
                }
            }
            // Période d'un mois 
            if(!$this->_startsAt || $this->_startsAt == null)
            {
                $this->_errorsMsg = $this->errorMessageParameterMissing();
                $this->_result = false;

                return $this;
            }

            $this->_result = true;
            return $this;
        }

        /*
         * - Message d'erreur du l'absence de la periode de facturation
         *
         */
        private function errorMessageParameterMissing()
        {
            return 'Veuillez renseigner une période de facturation pour poursuivre.';
        }
        /*
         * - Message d'erreur du l'incoherence entre les dates de debut et de fin de facturation
         *
         */
        private function IncoherenceErrorMessage()
        {
            return 'La date de fin de période doit être postérieure à la date de debut de facturation. Veuillez corriger la période pour poursuivre.';
        }
        /*
         * - Description de l'operation en cours
         *  retourne un string
         */
        public function _objectif()
        {
            $startAtText = strftime("%B", $this->_startsAt);
            $text = 'facturation du mois de '. $startAtText;

            if($this->_endsAt != null)
                return $text .' au mois de '. strftime("%B", $this->_endsAt);

            return $text;
        }

    }

    

    /**
    * - Gestionnaire de date
    */
    class dateHandler
    {
        
        function __construct(argument)
        {
            # code...
        }
        /*
         * - Creer un object Datetime
         *
         */
        public function datetimeObjectConverter($month, $year)
        {
            # code ...
            return $dateTime;
        }
        /*
         * - Retourne un array<{month: int, year: int, reference: datetime}>
         *
         */
        public function datetimeObjectDeconverter(\DateTime $datetime)
        {
            # code ...
            return [
                'month' => $month,
                'year' => $year,
                'reference' => $datetime
            ];
        }
    }





    
}
