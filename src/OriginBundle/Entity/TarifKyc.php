<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TarifKyc
 *
 * @ORM\Table(name="tarif_kyc")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\TarifKycRepository")
 */
class TarifKyc extends Tarif
{
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    public $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="$prix_standart", type="string", length=255)
     */
    public $prixStandart = '1500';

    /**
     * TarifKyc constructor.
     * @param string $nom
     */
    public function __construct($nom, $montant)
    {
        $this->nom = $nom;
        $this->flat = $montant;

    }


}

