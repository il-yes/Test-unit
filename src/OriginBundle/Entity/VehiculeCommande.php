<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\FabriqueOption;

/**
 * VehiculeCommande
 *
 * @ORM\Table(name="vehicule_commande")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\VehiculeCommandeRepository")
 */
class VehiculeCommande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     */
    private $options;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * VehiculeCommande constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @var array
     *
     * @ORM\Column(name="prix_de_vente_options", type="array")
     */
    private $prixDeVenteOptions;


    public function ajouterOptions($nom, $prixDeVente, FabriqueOption $fabrique)
    {
        $this->options[] = $fabrique->getOption($nom);
        $this->prixDeVenteOptions[] = $prixDeVente;
    }

    public function afficheOptions()
    {
        foreach ($this->options as $index => $option)
        {
            //dump($option);
            $option->affiche($this->prixDeVenteOptions[$index]);
        }
        //die('FIN');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set options
     *
     * @param array $options
     *
     * @return VehiculeCommande
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set prixDeVenteOptions
     *
     * @param array $prixDeVenteOptions
     *
     * @return VehiculeCommande
     */
    public function setPrixDeVenteOptions($prixDeVenteOptions)
    {
        $this->prixDeVenteOptions = $prixDeVenteOptions;

        return $this;
    }

    /**
     * Get prixDeVenteOptions
     *
     * @return array
     */
    public function getPrixDeVenteOptions()
    {
        return $this->prixDeVenteOptions;
    }
}
