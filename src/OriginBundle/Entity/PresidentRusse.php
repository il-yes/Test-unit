<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 11:27
 */

namespace OriginBundle\Entity;

use OriginBundle\Model\PersonneInterface;



class PresidentRusse implements PersonneInterface {

    public function parlerDuTemps() {
        echo 'Я очень рад быть здесь, погода прекрасна в Париже';
    }
}