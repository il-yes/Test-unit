<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OptionVehicule
 *
 * @ORM\Table(name="option_vehicule")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\OptionVehiculeRepository")
 */
class OptionVehicule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_standard", type="float", nullable=true)
     */
    private $prixStandard;

    /**
     * OptionVehicule constructor.
     * @param $nom
     */
    public function __construct($nom)
    {
        $this->nom = $nom;
        $this->description = "Description de : ". $nom;
        $this->prixStandard = 100;
    }

    public function affiche($prixVente)
    {
        echo "Nom : ". $this->nom . "<br/>".
             "Description :  : ". $this->nom . "<br/>".
             "Prix standard : ". $this->prixStandard . "<br/>".
             "Prix de vente : ". $prixVente. "<br/><hr>";
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return OptionVehicule
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OptionVehicule
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prixStandard
     *
     * @param float $prixStandard
     *
     * @return OptionVehicule
     */
    public function setPrixStandard($prixStandard)
    {
        $this->prixStandard = $prixStandard;

        return $this;
    }

    /**
     * Get prixStandard
     *
     * @return float
     */
    public function getPrixStandard()
    {
        return $this->prixStandard;
    }
}
