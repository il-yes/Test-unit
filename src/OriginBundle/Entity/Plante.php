<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\Flore;

/**
 * Plante
 *
 * @ORM\Table(name="plante")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\PlanteRepository")
 */
class Plante extends Flore
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    public function __construct($milieux) {
        $this->_environnement = $milieux;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function se_deplacer()
    {
        return "mouvements extremement lents imperceptibles a l'oeil humain";
    }


}

