<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\Config;
use OriginBundle\Model\ObservateurInterface;

/**
 * Tarif
 *
 * @ORM\Table(name="tarif")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\TarifRepository")
 */
class Tarif extends Config
{
	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Tarif constructor.
     * @param $nom
     */
    public function __construct($nom, $montant)
    {
        $this->nom = $nom;
        $this->flat = $montant;
    }


}

