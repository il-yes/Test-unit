<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 11:26
 */

namespace OriginBundle\Entity;

use OriginBundle\Model\PersonneInterface;




class InterpreteRusse extends Interprete implements PersonneInterface {

    public function parlerDuTemps() {

        if (null == $this->_president) {
            $this->_president = new PresidentRusse();
        }

        echo 'Au sujet du temps, le président russe me dit : "';
        $this->_president->parlerDuTemps();
        echo '"', PHP_EOL;
    }
}