<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\ObservateurInterface;
use OriginBundle\Model\SujetInterface;

/**
 * Fan
 *
 * @ORM\Table(name="fan")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\FanRepository")
 */
class Fan implements ObservateurInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    private $_sujet;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function mettreAJour(SujetInterface $sujet, \stdClass $objet, $options) {
        echo "Il y a eu du changement dans ", $sujet->getNom(), "\n";
        foreach($objet as $attr => $valeur) {
            if (is_array($valeur)) {
                $valeur = implode(',', $valeur);
            }
            echo "$attr vaut $valeur", PHP_EOL;
        }
    }
}

