<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\StrategyInterface;

/**
 * Strategy
 *
 * @ORM\Table(name="strategy")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\StrategyRepository")
 */
class Strategy implements StrategyInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    protected $data;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function talk($data)
    {
        $this->data = $data;
        return $this->data;
    }

}

