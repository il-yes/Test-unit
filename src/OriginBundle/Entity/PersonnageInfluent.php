<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\ObservateurInterface;
use OriginBundle\Model\SujetInterface;

/**
 * PersonnageInfluent
 *
 * @ORM\Table(name="personnage_influent")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\PersonnageInfluentRepository")
 */
class PersonnageInfluent implements SujetInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="integer")
     */
    private $age;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer")
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="hobbies", type="text")
     */
    private $hobbies;

    private $observateurs;

    private $_stockage;

    private $_options;

    /**
     * PersonnageInfluent constructor.
     * @param $nom
     */
    public function __construct($nom)
    {
        $this->nom = $nom;
        $this->_stockage = new \stdClass();
        $this->_options = new \stdClass();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return PersonnageInfluent
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return PersonnageInfluent
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return PersonnageInfluent
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set hobbies
     *
     * @param string $hobbies
     *
     * @return PersonnageInfluent
     */
    public function setHobbies($hobbies)
    {
        $this->hobbies = $hobbies;

        return $this;
    }

    /**
     * Get hobbies
     *
     * @return string
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }

    public function attacher(ObservateurInterface $observateur)
    {
        $this->observateurs[] = $observateur;
    }

    public function detacher(ObservateurInterface $observateur)
    {
        $key = array_search($observateur, $this->observateurs);

        if (false !== $key) {
            unset($this->observateurs[$key]);
        }
    }

    public function notifier()
    {
        foreach ($this->observateurs as $observateur) {

            $observateur->mettreAJour($this, $this->_stockage, $this->_options);
        }
    }

    public function changerAge($age) {
        $this->age = $age;
        $this->_stockage->age = $age;
    }

    public function changerHobbies(Array $hobbies) {
        $this->hobbies = $hobbies;
        $this->_stockage->hobbie = $hobbies;
    }

    /**
     * @return \stdClass
     */
    public function getStockage()
    {
        return $this->_stockage;
    }

    /**
     * @param \stdClass $stockage
     */
    public function setStockage($stockage)
    {
        $this->_stockage = $stockage;
    }

    /**
     * @return \stdClass
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * @param \stdClass $options
     */
    public function setOptions($options)
    {
        $this->_options = $options;
    }


}

