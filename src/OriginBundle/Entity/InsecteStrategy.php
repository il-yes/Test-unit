<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\StrategyInterface;

/**
 * InsecteStrategy
 *
 * @ORM\Table(name="insecte_strategy")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\InsecteStrategyRepository")
 */
class InsecteStrategy implements StrategyInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function talk($data)
    {
        echo $data . 'Je suis un insecte ';
    }
}

