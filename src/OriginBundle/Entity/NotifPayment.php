<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotifPayment
 *
 * @ORM\Table(name="notif_payment")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\NotifPaymentRepository")
 */
class NotifPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="success", type="string", length=255)
     */
    private $success;

    /**
     * @var string
     *
     * @ORM\Column(name="echec", type="string", length=255)
     */
    private $echec;

    /**
     * @var string
     *
     * @ORM\Column(name="recall", type="string", length=255)
     */
    private $recall;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set success
     *
     * @param string $success
     *
     * @return NotifPayment
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set echec
     *
     * @param string $echec
     *
     * @return NotifPayment
     */
    public function setEchec($echec)
    {
        $this->echec = $echec;

        return $this;
    }

    /**
     * Get echec
     *
     * @return string
     */
    public function getEchec()
    {
        return $this->echec;
    }

    /**
     * Set recall
     *
     * @param string $recall
     *
     * @return NotifPayment
     */
    public function setRecall($recall)
    {
        $this->recall = $recall;

        return $this;
    }

    /**
     * Get recall
     *
     * @return string
     */
    public function getRecall()
    {
        return $this->recall;
    }
}

