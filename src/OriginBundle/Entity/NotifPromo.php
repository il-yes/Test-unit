<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotifPromo
 *
 * @ORM\Table(name="notif_promo")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\NotifPromoRepository")
 */
class NotifPromo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="success", type="string", length=255)
     */
    private $success;

    /**
     * @var string
     *
     * @ORM\Column(name="echec", type="string", length=255)
     */
    private $echec;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set success
     *
     * @param string $success
     *
     * @return NotifPromo
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set echec
     *
     * @param string $echec
     *
     * @return NotifPromo
     */
    public function setFail($echec)
    {
        $this->echec = $echec;

        return $this;
    }

    /**
     * Get echec
     *
     * @return string
     */
    public function getEchec()
    {
        return $this->echec;
    }


}

