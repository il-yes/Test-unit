<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\Config;

/**
 * Clt
 *
 * @ORM\Table(name="clt")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\CltRepository")
 */
class Clt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    public $configs = [];
    /**
     * Clt constructor.
     * @param string $nom
     */
    public function __construct($nom)
    {
        $this->nom = $nom;
    }


    public function attacher(Tarif $tarif)
    {
        $this->configs[] = $tarif;
    }

    public function detacher(Tarif $tarif)
    {
        $key = array_search($tarif, $this->configs);

        if (false !== $key) {
            unset($this->configs[$key]);
        }
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Clt
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}

