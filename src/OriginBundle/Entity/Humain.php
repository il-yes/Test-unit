<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\EtreHumain;

/**
 * Humain
 *
 * @ORM\Table(name="humain")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\HumainRepository")
 */
class Humain extends EtreHumain
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    public function __construct($identite) {
        $this->_nationalite = $identite;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function respirer() {
        echo 'Par le nez et/ou la bouche';
    }

    protected function _cesserFonctionsVitales() {
        echo 'le coeur s\'arrête de battre...AAAARGH';
    }

    public function nager() {
        echo 'Je peux aussi nager si on m\'apprends';
    }

    public function courir() {
        echo 'Je passe une jambe devant l\'autre, très vite';
    }
}

