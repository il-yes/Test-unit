<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 11:28
 */

namespace OriginBundle\Entity;



class PresidentFrancais
{
    private $_interprete;

    public function attacherInterprete(Interprete $interprete) {
        $this->_interprete = $interprete;
    }

    public function discuterSurPerron() {
        if (!$this->_interprete) {
            throw new \RuntimeException('Où est l\'interprète ?');
        }

        echo "Président, il fait bon vivre à Paris, hein ?", PHP_EOL;
        $this->_interprete->parlerDuTemps();
    }
}