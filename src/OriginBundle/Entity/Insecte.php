<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\Bouger;
use OriginBundle\Model\EtreCoureur;
use OriginBundle\Model\EtreNageur;
use OriginBundle\Model\Faune;
use OriginBundle\Model\StrategyInterface;

/**
 * Insecte
 *
 * @ORM\Table(name="insecte")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\InsecteRepository")
 */
class Insecte extends Faune implements Bouger, EtreNageur, EtreCoureur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function courir()
    {
        // TODO: Implement courir() method.
    }

    public function nager()
    {
        // TODO: Implement nager() method.
    }

    public function se_deplacer()
    {
        // TODO: Implement se_deplacer() method.
    }


}

