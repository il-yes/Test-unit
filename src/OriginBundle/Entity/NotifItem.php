<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\NotificationShop as BaseNotification;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * NotifItem
 *
 * @ORM\Table(name="notif_item")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\NotifItemRepository")
 */
class NotifItem extends BaseNotification implements ContainerInterface
{
     private $shop_add_item;

     private $shop_remove_item;

     private $shop_update_item;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="addTo", type="string", length=255)
     */
    private $addTo;

    /**
     * @var string
     *
     * @ORM\Column(name="removeFrom", type="string", length=255)
     */
    private $removeFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="toUpdate", type="string", length=255)
     */
    private $toUpdate;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get addTo
     *
     * @return string
     */
    public function getAddTo()
    {
        return $this->addTo = "Hello my friend !!! Your item was added :)";
    }


    /**
     * Get removeFrom
     *
     * @return string
     */
    public function getRemoveFrom()
    {
        return $this->removeFrom = "Thanks even though you leave this one :)";
    }


    /**
     * Get toUpdate
     *
     * @return string
     */
    public function getToUpdate()
    {
        return $this->toUpdate = "Good news ! Ypur item was update :)";
    }

    /**
     * @return mixed
     */
    public function getShopAddItem()
    {
        return $this->container->getParameter('app.shop_item_add');
    }

    /**
     * @param mixed $shop_add_item
     */
    public function setShopAddItem($shop_add_item)
    {
        $this->shop_add_item = $shop_add_item;
    }

    /**
     * @return mixed
     */
    public function getShopRemoveItem()
    {
        return $this->container->getParameter('app.shop_item_remove');
    }

    /**
     * @param mixed $shop_remove_item
     */
    public function setShopRemoveItem($shop_remove_item)
    {
        $this->shop_remove_item = $shop_remove_item;
    }

    /**
     * @return mixed
     */
    public function getShopUpdateItem()
    {
        return $this->container->getParameter('app_shop.item_edit');
    }

    /**
     * @param mixed $shop_update_item
     */
    public function setShopUpdateItem($shop_update_item)
    {
        $this->shop_update_item = $shop_update_item;
    }

    protected function _displayMessage($options)
    {
        return $this->param_to_action($options);
    }

    public function param_to_action($action)
    {

        switch ($action) {
            case $this->getShopAddItem():
                return $this->getAddTo();
                break;
            case $this->getShopRemoveItem():
                return $this->getRemoveFrom();
                break;
            case $this->getShopUpdateItem():
                return $this->getToUpdate();
                break;
        }
    }

    private function getActionParameter($name)
    {
        return $this->container->getParameter($name);
    }

    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }


}

