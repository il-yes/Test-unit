<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 12:43
 */

namespace OriginBundle\Entity;

use OriginBundle\Model\ImageAbstract;



class StandardImage extends ImageAbstract {

    public function __construct($cheminFichier) {

        parent::__construct($cheminFichier);

        $this->_contenuFichier = file_get_contents($this->_cheminFichier);

        $this->_tailleFichier = filesize($this->_cheminFichier);
    }

    public function afficherContenu() {
        return $this->_contenuFichier;
    }


}