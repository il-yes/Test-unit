<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\UserRepository")
 */
class User
{
    public function __construct()
    {
        // your own logic
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="gender_code", type="string", length=255)
     */
    private $genderCode;

    /**
     * @ORM\OneToMany(targetEntity="OriginBundle\Entity\Messages", mappedBy="author", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $messages;


    /**
     * @ORM\OneToOne(targetEntity="OriginBundle\Entity\Image", cascade={"persist", "remove"})
     */
    private $avatar;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set genderCode
     *
     * @param string $genderCode
     *
     * @return User
     */
    public function setGenderCode($genderCode)
    {
        $this->genderCode = $genderCode;

        return $this;
    }

    /**
     * Get genderCode
     *
     * @return string
     */
    public function getGenderCode()
    {
        return $this->genderCode;
    }

    /**
     * Add message
     *
     * @param \OriginBundle\Entity\Messages $message
     *
     * @return User
     */
    public function addMessage(\OriginBundle\Entity\Messages $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \OriginBundle\Entity\Messages $message
     */
    public function removeMessage(\OriginBundle\Entity\Messages $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set avatar
     *
     * @param \OriginBundle\Entity\Image $avatar
     *
     * @return User
     */
    public function setAvatar(\OriginBundle\Entity\Image $avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return \OriginBundle\Entity\Image
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
}
