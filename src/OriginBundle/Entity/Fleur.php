<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OriginBundle\Model\StrategyInterface;

/**
 * Fleur
 *
 * @ORM\Table(name="fleur")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\FleurRepository")
 */
class Fleur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}

