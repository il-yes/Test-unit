<?php

namespace OriginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TarifAtm
 *
 * @ORM\Table(name="tarif_atm")
 * @ORM\Entity(repositoryClass="OriginBundle\Repository\TarifAtmRepository")
 */
class TarifAtm extends Tarif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    public $nom;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return TarifAtm
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="$prix_standart", type="string", length=255)
     */
    public $prixStandart = '900';

    /**
     * TarifKyc constructor.
     * @param string $nom
     */
    public function __construct($nom,$montant)
    {
        $this->nom = $nom;
        $this->flat = $montant;
    }
}

