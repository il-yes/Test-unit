<?php
/**
 * Created by PhpStorm.
 * User: manuel
 * Date: 07/08/17
 * Time: 12:45
 */

namespace OriginBundle\Entity;

use OriginBundle\Model\ImageAbstract;



class ProxyImage extends ImageAbstract {

    private $_vraieImage;

    public function __construct($cheminFichier) {

        parent::__construct($cheminFichier);

        $this->_tailleFichier = filesize($this->_cheminFichier);
        $this->_tailleFichier = filesize($this->_cheminFichier);
    }

    public function afficherContenu() {

        if (!$this->_vraieImage) {
            $this->_vraieImage = new StandardImage($this->_cheminFichier);
        }
        $this->_vraieImage->afficherContenu();
    }
}