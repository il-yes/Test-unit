<?php

namespace OriginBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectManager;
use OriginBundle\Entity\Messages;
use OriginBundle\Entity\Category;
use OriginBundle\Entity\Image;
use OriginBundle\Entity\Tags;
use OriginBundle\Entity\User;

class LoadMessagesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
	    $sujet = ["php","javascript","symfony2","angular Js","web design","developpement","integration","jquery"];
	    $text = [
	    	"Lorem ipsum dolor sit amet, consectetur adipiscing elit.", 
	    	"Morbi lacinia venenatis lorem non commodo. Maecenas ut metus purus.",
	    	"Suspendisse vel erat non dolor aliquet condimentum. Proin mollis sapien urna, vel dapibus diam vestibulum ut. ",
	    	"Aenean vitae odio a nisi euismod commodo et sit amet nunc. Etiam tortor eros, maximus sed est a, egestas condimentum purus. Vivamus nec lorem nunc. Phasellus iaculis luctus magna, sed pulvinar lacus. Fusce scelerisque est quis nibh pellentesque, ut dictum dolor suscipit.",
			"Sed hendrerit sit amet nisl mollis pulvinar. ",
			"Nunc consectetur eros et libero auctor condimentum. Donec lacus lectus, condimentum vitae eros at, vulputate lobortis augue. Duis at sapien ut ex egestas pretium sed sed lorem. Duis bibendum dapibus metus non dignissim. Nulla facilisi.", 
			"Quisque congue eu enim et fermentum. ",
			"Pellentesque rhoncus enim id facilisis varius. Sed ac tortor in ipsum sodales posuere ut eu metus. ",
			"Vestibulum viverra odio justo, in iaculis massa rhoncus a. Aliquam purus dolor, malesuada id mattis eu, rhoncus ut massa. ",
			"Fusce scelerisque enim magna, et consectetur est auctor eu. Morbi in consectetur sapien, nec aliquam nisl. ",
			"Integer volutpat fermentum accumsan. Maecenas sit amet ultricies odio. ",
			"Sed quis erat at metus scelerisque hendrerit.",
			"Sed quis molestie orci. Donec vitae neque nec nulla convallis suscipit. ",
			"Fusce imperdiet, magna eget elementum tempor, diam augue mattis dui, sed ullamcorper leo orci eget quam. Curabitur interdum massa in massa varius, quis sollicitudin leo malesuada. Quisque pellentesque nunc ac sem tincidunt pellentesque. Vivamus pulvinar, ex vel vulputate convallis, erat magna porta arcu, a posuere elit tortor a metus. Nunc iaculis fermentum justo, et tristique nunc fringilla vel.",
			"Nunc ullamcorper volutpat volutpat. Duis blandit, leo quis ultricies vestibulum, purus risus efficitur ante, in molestie arcu neque vitae urna. Etiam ornare auctor pulvinar. Ut non condimentum urna, vitae blandit felis.", 
			"Praesent commodo pulvinar orci vestibulum commodo. In eu vehicula lacus, tempus efficitur risus. Integer suscipit tortor vel gravida dictum. Quisque eget erat non est consequat gravida. Integer et eros congue, convallis ante vestibulum, auctor mi. Duis imperdiet lacus id mattis viverra. Praesent tortor erat, aliquam quis venenatis at, suscipit vel odio. Vestibulum eleifend dolor eu convallis congue. Nunc ac diam auctor, semper lorem eget, cursus justo. Proin ultrices mi eu lorem eleifend, ut ullamcorper orci ullamcorper."
		];


	    for($i =0; $i <= 50; $i++)
	    {

	    	//$randSujet = rand(0,7);
		    //$randText = rand(0,14);
	  

	  		$message = new Messages();
		    $message->setSubject($author[rand(0,7)]);
		    $message->setMessage($text[rand(0,15)]);
		      
		    
		 	$manager->persist($message);
		
		 	// L'entité ayant l'option "cascade:persist" sera persisité en meme temps que l'objet Messages

		 	$manager->flush();
	    }
	}

        
}