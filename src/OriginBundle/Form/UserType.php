<?php

namespace OriginBundle\Form;

use OriginBundle\Form\Type\GenderType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                "label" => "Auteur"
            ])
            ->add('genderCode', GenderType::class, [
                'placeholder' => 'Choisissez le genre',
                "label" => "Sexe"
            ])
            ->add('avatar', new ImageType(), [
                "label" => false
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OriginBundle\Entity\User'
        ));
    }
}
