<?php

namespace IntBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    public function indexAction(Request $request)
    {
        $locale = $request->getLocale();
        //die(Debug::dump($locale));
        $products = $this->getDoctrine()->getManager()->getRepository('IntBundle:Product')->myFindAllByLocale($locale);


        return $this->render('IntBundle:Product:index.html.twig', [
            'products' => $products
        ]);
    }
}