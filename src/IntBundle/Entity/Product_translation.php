<?php

namespace IntBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product_translation
 *
 * @ORM\Table(name="product_translation")
 * @ORM\Entity(repositoryClass="IntBundle\Repository\Product_translationRepository")
 */
class Product_translation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keys_words", type="string", length=255, nullable=true)
     */
    private $metaKeysWords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="description_short", type="string", length=255, nullable=true)
     */
    private $descriptionShort;


    /**
     * @var string
     *
     * @ORM\Column(name="langue", type="string", length=255, nullable=true)
     */
    private $langue;


    /**
     * @ORM\ManyToOne(targetEntity="IntBundle\Entity\Product", inversedBy="translations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $product;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product_translation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product_translation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaKeysWords
     *
     * @param string $metaKeysWords
     *
     * @return Product_translation
     */
    public function setMetaKeysWords($metaKeysWords)
    {
        $this->metaKeysWords = $metaKeysWords;

        return $this;
    }

    /**
     * Get metaKeysWords
     *
     * @return string
     */
    public function getMetaKeysWords()
    {
        return $this->metaKeysWords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return Product_translation
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set descriptionShort
     *
     * @param string $descriptionShort
     *
     * @return Product_translation
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;

        return $this;
    }

    /**
     * Get descriptionShort
     *
     * @return string
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * Set product
     *
     * @param \IntBundle\Entity\Product $product
     *
     * @return Product_translation
     */
    public function setProduct(\IntBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \IntBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set langue
     *
     * @param string $langue
     *
     * @return Product_translation
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }
}
