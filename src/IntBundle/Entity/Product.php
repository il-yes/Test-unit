<?php

namespace IntBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="IntBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;


    /**
     * @ORM\OneToMany(targetEntity="IntBundle\Entity\Product_translation", mappedBy="product")
     */
    private $translations;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->codeLocale = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add codeLocale
     *
     * @param \IntBundle\Entity\Locale $codeLocale
     *
     * @return Product
     */
    public function addCodeLocale(\IntBundle\Entity\Locale $codeLocale)
    {
        $this->codeLocale[] = $codeLocale;

        return $this;
    }

    /**
     * Remove codeLocale
     *
     * @param \IntBundle\Entity\Locale $codeLocale
     */
    public function removeCodeLocale(\IntBundle\Entity\Locale $codeLocale)
    {
        $this->codeLocale->removeElement($codeLocale);
    }

    /**
     * Get codeLocale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCodeLocale()
    {
        return $this->codeLocale;
    }

    /**
     * Get codeLocales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCodeLocales()
    {
        return $this->codeLocales;
    }


    /**
     * Add translation
     *
     * @param \IntBundle\Entity\Product_translation $translation
     *
     * @return Product
     */
    public function addTranslation(\IntBundle\Entity\Product_translation $translation)
    {
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \IntBundle\Entity\Product_translation $translation
     */
    public function removeTranslation(\IntBundle\Entity\Product_translation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}
