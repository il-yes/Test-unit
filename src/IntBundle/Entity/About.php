<?php

namespace IntBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * About
 *
 * @ORM\Table(name="about")
 * @ORM\Entity(repositoryClass="IntBundle\Repository\AboutRepository")
 */
class About
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="IntBundle\Entity\About_translation", mappedBy="about")
     */
    private $aboutTranslations;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->aboutTranslations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add aboutTranslation
     *
     * @param \IntBundle\Entity\About_translation $aboutTranslation
     *
     * @return About
     */
    public function addAboutTranslation(\IntBundle\Entity\About_translation $aboutTranslation)
    {
        $this->aboutTranslations[] = $aboutTranslation;

        return $this;
    }

    /**
     * Remove aboutTranslation
     *
     * @param \IntBundle\Entity\About_translation $aboutTranslation
     */
    public function removeAboutTranslation(\IntBundle\Entity\About_translation $aboutTranslation)
    {
        $this->aboutTranslations->removeElement($aboutTranslation);
    }

    /**
     * Get aboutTranslations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAboutTranslations()
    {
        return $this->aboutTranslations;
    }
}
